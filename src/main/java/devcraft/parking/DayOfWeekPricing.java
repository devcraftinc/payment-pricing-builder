//Copyright DevCraft, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package devcraft.parking;

import static java.time.temporal.ChronoUnit.DAYS;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.util.HashMap;
import java.util.Map;

public class DayOfWeekPricing implements Pricing {

	private final Map<DayOfWeek, Pricing> days = new HashMap<DayOfWeek, Pricing>();
	
	public void setPricing(DayOfWeek day, Pricing pricing) {
		days.put(day, pricing);
	}

	public int calculatePayment(Instant entryTime, Instant paymentTime, ZoneOffset zoneOffset) {
		int payment = 0;
		Instant nextDayStart = nextDayStart(entryTime, zoneOffset);
		while (nextDayStart.isBefore(paymentTime)) {
			payment += getPaymentPolicy(entryTime, zoneOffset).calculatePayment(entryTime, nextDayStart, zoneOffset);
			entryTime = nextDayStart;
			nextDayStart = nextDayStart.plus(1, DAYS);
		}
		payment += getPaymentPolicy(entryTime, zoneOffset).calculatePayment(entryTime, paymentTime, zoneOffset);
		return payment;
	}

	private Pricing getPaymentPolicy(Instant entryTime, ZoneOffset zoneOffset) {
		ZonedDateTime zonedEntryTime = ZonedDateTime.ofInstant(entryTime, zoneOffset);
		DayOfWeek dayOfWeek = DayOfWeek.of(zonedEntryTime.get(ChronoField.DAY_OF_WEEK));
		return getPaymentPolicy(dayOfWeek);
	}
	
	protected Pricing getPaymentPolicy(DayOfWeek dayOfWeek) {
		return days.get(dayOfWeek);
	}
	
	private Instant nextDayStart(Instant entryTime, ZoneOffset zone) {
		ZonedDateTime zonedEntryTime = ZonedDateTime.ofInstant(entryTime, zone);
		LocalDate dayOfEntry = zonedEntryTime.toLocalDate();
		ZonedDateTime nextDayStart = ZonedDateTime.of(dayOfEntry, LocalTime.of(6, 0), zone);
		if (!nextDayStart.isAfter(zonedEntryTime))
			nextDayStart = nextDayStart.plusDays(1);
		return nextDayStart.toInstant();
	}
}